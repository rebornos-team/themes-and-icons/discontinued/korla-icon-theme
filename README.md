# korla-icon-theme

SVG icon theme suitable for every desktop environment (dark and light versions, HiDPI support)

https://github.com/bikass/korla

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/themes-and-icons/korla-icon-theme.git
```

Do not use epoch:

```
#epoch=1
```

